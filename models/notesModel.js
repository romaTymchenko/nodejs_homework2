const mongoose = require('mongoose');

const notesSchema = new mongoose.Schema({
  text: {
    type: String,
    required: true,
  },

  userId: {
    type: String,
    required: true,
  },

  createdDate: {
    type: Date,
    default: Date.now(),
  },
  completed: {
    type: Boolean,
    default: false,
  },
});

module.exports.Note = mongoose.model('Note', notesSchema);
