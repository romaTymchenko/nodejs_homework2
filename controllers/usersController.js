const bcrypt = require('bcrypt');
const {User} = require('../models/userModel');


module.exports.getUserInfo = async (req, res) => {
  const myUser = await User.findById(
      req.user._id,
      'username createdDate');


  res.status(200).json({user: {
    _id: myUser._id,
    username: myUser.username,
    createdDate: myUser.createdDate}});
};

module.exports.deleteUser = async (req, res) => {
  await User.findByIdAndRemove(req.user._id, function(err, data) {
    res.status(200).json({message: 'Success'});
  });
};

module.exports.changeUserPassword = async (req, res) => {
  const {oldPassword, newPassword} = req.body;
  const user = await User.findById(req.user._id, 'password').exec();

  if (!(await bcrypt.compare(oldPassword, user.password))) {
    return res.status(400).json({message: 'wrong password'});
  }
  user.password = await bcrypt.hash(newPassword, 10);
  await user.save();

  res.status(200).json({message: 'Success'});
};
