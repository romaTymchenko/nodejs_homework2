const {Note} = require('../models/notesModel');


module.exports.addNote = async (req, res) => {
  //  myUser =await User.findById(req.user._id,'username createdDate').exec();
  const {text} = req.body;
  console.log(text, req.user);
  const userId = req.user._id;

  if (!text) {
    return res.status(400).json({message: `Text not found`});
  }

  const note = new Note({
    userId,
    text,
  });

  await note.save();
  res.status(200).json({message: 'Note created successfully'});
};

module.exports.getAllNotes = async (req, res) => {
  const userId = req.user._id;
  const {skip = 0, limit = 5} = req.query;
  const notes = await Note.find(
      {userId},
      ['userId', 'text', 'createdDate', 'completed'], {
        skip,
        limit: +limit > 100 ? 5 : +limit,
        sort: {
          createdDate: -1,
        },
      });

  res.status(200).json({notes: notes});
};

module.exports.getUserNote = async (req, res) => {
  const {id} = req.params;


  // let myNote = await Note.findById(id,
  //   ['userId', 'text', 'createdDate', 'completed']).exec();

  const myNote = await Note.findById(id, {_v: 0}).exec();

  res.status(200).json({note: {
    _id: myNote._id,
    userId: myNote.userId,
    completed: myNote.completed,
    text: myNote.text,
    createdDate: myNote.createdDate}});
};

module.exports.deleteUserNote = async (req, res) => {
  const {id} = req.params;

  await Note.findByIdAndRemove(id, req.body, function(err, data) {
    if (!err) {
      res.status(200).json({message: 'Success'});
    }
  });
};

module.exports.editUserNote = async (req, res) => {
  const {id} = req.params;
  const {text} = req.body;
  const myNote = await Note.findById(id, {_v: 0}).exec();
  myNote.text = text;
  await myNote.save();
  res.status(200).json({message: 'Success'});
};

module.exports.swichCheckUserNote = async (req, res) => {
  const {id} = req.params;
  const myNote = await Note.findById(id, {_v: 0}).exec();
  myNote.completed = myNote.completed === true ? false : true;
  await myNote.save();
  res.status(200).json({message: 'Success'});
};
